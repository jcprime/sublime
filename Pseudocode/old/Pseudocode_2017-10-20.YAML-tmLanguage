# [PackageDev] target_format: plist, ext: tmLanguage
---
name: Pseudocode
scopeName: source.pseudocode
fileTypes: [pc]
uuid: d33c939f-f1e6-4807-832e-9da0d6c46eab

patterns:
- name: comment.line.note.pseudocode
  match: (//|#).*$\n?
  captures:
    '1': {name: punctuation.definition.comment.pseudocode}

- name: keyword.control.conditional.pseudocode
  match: \b([I]f|[E]lse [I]f|[E]nd [Ii]f|[T]hen|[E]lse|[S]elect|[C]ase)\b

- name: keyword.control.for.pseudocode
  match: \b([F]or|IN|[E]nd [Ff]or)\b

- name: keyword.control.while.pseudocode
  match: \b([D]o|[W]hile|[E]nd [Ww]hile)\b

- name: keyword.control.module.pseudocode
  match: \b([E]nd [Mm]odule)\b

- name: keyword.control.function.pseudocode
  match: \b([Dd]efine|[Ff]unction|[E]nd [Ff]unction)\b

- name: keyword.control.class.pseudocode
  match: \b([Cc]lass|[Ee]nd [Cc]lass)\b

# - name: entity.name.function.pseudocode
#   match: ([A-Za-z_][A-Za-z0-9_]*)(\(\))
#   captures:
#     '2': {name: punctuation.definition.parameters.begin.pseudocode}

# - name: entity.name.class.pseudocode
#   match: ([A-Za-z][A-Za-z_]*)(?=[\(\):])
#   captures:
#     '2': {name: punctuation.definition.parameters.begin.pseudocode}

- name: storage.type.pseudocode
  match: \b([Ii]nteger|[Rr]eal|[Ff]loat|[Ss]tring|[Bb]oolean)\b

- name: storage.modifier.pseudocode
  match: \b([Cc]onstant|[Rr]ef|[Pp]ointer)\b

- name: constant.numeric.pseudocode
  match: \b((0(x|X)[0-9a-fA-F]+)|([0-9]+(\.[0-9]+)?))\b

- name: keyword.operator.pseudocode
  match: =|==|!=|AND|OR|NOT|XOR|&&|\|\||>|<|>>|<<|\|

- name: keyword.boolean.pseudocode
  match: ([Tt][Rr][Uu][Ee])|([Ff][Aa][Ll][Ss][Ee])

- name: keyword.null.pseudocode
  match: ([Nn][Uu][Ll][Ll])

- name: keyword.variable.pseudocode
  match: (\$\{)([A-Za-z0-9_\(\)\"\+\-\/\*\%\^\[\]\:\.\s]*)(\})
  #match: (\$\{)([^\$]*)(\})
  captures:
    '1': {name: punctuation.definition.parameters.begin.pseudocode}
    '2': {name: variable.pseudocode}
    '3': {name: punctuation.definition.parameters.end.pseudocode}

- name: keyword.other.pseudocode
  match: \b([D]isplay|[I]nput|[D]eclare|[S]et|[C]all|[R]eturn|[P]rint|[O]pen|[O]utput|[P]ipe|[R]edirect|[A]ssign|[I]ncrement|[D]ecrement|[T]est)\b

- name: meta.module.pseudocode
  begin: ^\s*([Mm]odule)\s+(?=[A-Za-z_][A-Za-z0-9_]*\s*\()
  beginCaptures:
    '1': {name: storage.type.module.pseudocode}
  end: (\))
  endCaptures:
    '1': {name: punctuation.definition.parameters.end.pseudocode}
  patterns:
  - contentName: entity.name.function.pseudocode
    begin: (?=[A-Za-z_][A-Za-z0-9_]*)
    end: (?![A-Za-z0-9_])
  - contentName: meta.function.parameters.pseudocode
    begin: (\()
    beginCaptures:
      '1': {name: punctuation.definition.parameters.begin.pseudocode}
    end: (?=\)\s*)

- name: meta.function.pseudocode
  begin: ^\s*([Ff]unction)\s+(?=\w.*\s+)(?=[A-Za-z_][A-Za-z0-9_]*\s*)(\()
  beginCaptures:
    '1': {name: storage.type.function.pseudocode}
    '3': {name: entity.name.function.pseudocode}
    '4': {name: punctuation.definition.parameters.begin.pseudocode}
  end: (\))
  endCaptures:
    '1': {name: punctuation.definition.parameters.end.pseudocode}
  patterns:
  - contentName: meta.function.identifier.pseudocode
    begin: (?=[A-Za-z_][A-Za-z0-9_]*)\s*(\()
    end: (\))
    beginCaptures:
      '1': {name: entity.name.function.pseudocode}
      '2': {name: punctuation.definition.parameters.begin.pseudocode}
    endCaptures:
      '1': {name: punctuation.definition.parameters.end.pseudocode}
  - contentName: meta.function.return-type.pseudocode
    begin: (?=\w.*\s*\w+\s*[A-Za-z_][A-Za-z0-9_]*\s*\()
    end: (?=[A-Za-z_][A-Za-z0-9_]*\s*\()
    patterns:
    - name: storage.type.pseudocode
      match: \b([Ii]nteger|[Rr]eal|[Ss]tring|[Bb]oolean)\b

- name: meta.class.pseudocode
  begin: ^\s*([Cc]lass)\s+(?=\w.*\s+)(?=[A-Za-z][A-Za-z_]*\s*)(\()
  beginCaptures:
    '1': {name: keyword.control.class.pseudocode}
    '3': {name: entity.name.class.pseudocode}
    '4': {name: punctuation.definition.parameters.begin.pseudocode}
  end: (?=[\):])
  endCaptures:
    '1': {name: punctuation.definition.parameters.end.pseudocode}
  patterns:
  - contentName: entity.name.class.pseudocode
    begin: (?=[A-Za-z][A-Za-z_]*)
    end: (?![A-Za-z_])
  - contentName: meta.class.parameters.pseudocode
    begin: (\()
    beginCaptures:
      '1': {name: punctuation.definition.parameters.begin.pseudocode}
    end: (?=\)\s*)
  # - contentName: meta.class.identifier.pseudocode
  #   begin: (?=[A-Za-z][A-Za-z_]*)\s*(\()
  #   end: (\))
  #   beginCaptures:
  #     '1': {name: entity.name.class.pseudocode}
  #     '2': {name: punctuation.definition.parameters.begin.pseudocode}
  #   endCaptures:
  #     '1': {name: punctuation.definition.parameters.end.pseudocode}

- name: meta.while.pseudocode
  match: \s*([W]hile)\s+(\()((\w+\s?)*)\s*(\))
  captures:
    '1': {name: keyword.control.while.pseudocode}
    '2': {name: punctuation.definition.parameters.begin.pseudocode}
    '3': {name: meta.while.parameters.pseudocode}
    '5': {name: punctuation.definition.parameters.end.pseudocode}
  patterns:
  - contentName: meta.while.parameters.pseudocode
    match: ((\w+\s?)*)
    begin: \s*([W]hile)\s+(\()
    end: \s*(\))

- name: meta.for.pseudocode
  match: ^\s*([Ff]or)\s+(([\S]+[\s]*)*)\s+(IN)\s+(.*)
  captures:
    '1': {name: keyword.control.for.pseudocode}
    '2': {name: meta.for.parameters.pseudocode}
    # '3': {name: constant.numeric.pseudocode}
    '4': {name: keyword.control.for.pseudocode}
    '5': {name: meta.for.parameters.pseudocode}
    # '6': {name: keyword.operator.pseudocode}
    #'': {name: punctuation.definition.parameters.begin.pseudocode}
  patterns:
  # - include: $self
  - contentName: meta.for.parameters.pseudocode
    begin: \s*([F]or)\s+
    end: \s+(IN)
  - contentName: keyword.control.for.pseudocode
    match: \s+(IN)\s+
  # - contentName: meta.for.parameters.pseudocode
  #   begin: (IN)\s+
  #   end: ()

- name: meta.conditional.pseudocode
  match: ^\s*([I]f)\s+(\()(([\w\(\)]+\s?)*)\s*(\))\s+([T]hen)$
  captures:
    '1': {name: keyword.control.conditional.pseudocode}
    '2': {name: punctuation.definition.parameters.begin.pseudocode}
    '3': {name: meta.conditional.parameters.pseudocode}
    '5': {name: punctuation.definition.parameters.end.pseudocode}
    '6': {name: keyword.control.conditional.pseudocode}
  contentName: meta.conditional.parameters.pseudocode
  begin: ^\s*([I]f)\s+(\()
  end: \s*(\))\s+([T]hen)$

# The below is commented out so I can use contractions
# - name: string.quoted.single.pseudocode
#   begin: (')
#   beginCaptures:
#     '1': {name: punctuation.definition.string.begin.pseudocode}
#   end: (')
#   endCaptures:
#     '1': {name: punctuation.definition.string.end.pseudocode}

- name: string.quoted.double.pseudocode
  begin: (")
  beginCaptures:
    '1': {name: punctuation.definition.string.begin.pseudocode}
  end: (")
  endCaptures:
    '1': {name: punctuation.definition.string.end.pseudocode}
